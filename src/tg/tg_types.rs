#[derive(Debug, Deserialize, Clone)]
pub struct Chat {
    pub id: u64,
    pub username: Option<String>
}

#[derive(Debug, Deserialize, Clone)]
pub struct Sticker {
    pub file_id: String,
    pub set_name: Option<String>
}

#[derive(Debug, Deserialize, Clone)]
pub struct Message {
    pub message_id: u64,
    pub chat: Chat,
    pub text: Option<String>,
    pub sticker: Option<Sticker>
}

#[derive(Debug, Deserialize, Clone)]
pub struct User {
    pub id: u64,
    pub username: Option<String>
}

#[derive(Debug, Deserialize, Clone)]
pub struct InlineQuery {
    pub id: String,
    pub from: User,
    pub query: String,
    pub offset: String
}

#[derive(Debug, Deserialize, Clone)]
pub struct TgUpdate {
    pub update_id: u64,
    pub message: Option<Message>,
    pub inline_query: Option<InlineQuery>
}
