use types::Result;

use rusqlite::{Connection, Transaction};
use rusqlite::types::Null;


pub struct Db {
    connection: Connection,
}

impl Db {
    pub fn new(db_path: &str) -> Result<Db> {
        let db = Db {
            connection: Connection::open(&db_path)?,
        };
        db.connection.set_prepared_statement_cache_capacity(50);
        Ok(db)
    }

    pub fn create_tables(&self) -> Result<()> {
        self.connection.execute_batch(include_str!(
            "../migrations/20170822111315_create_stickers/up.sql"
        ))?;
        Ok(())
    }

    pub fn in_memory() -> Result<Db> {
        Ok(Db {
            connection: Connection::open_in_memory()?,
        })
    }

    fn get_sticker_id_by_file_id(transaction: &Transaction, file_id: &str, sticker_set: Option<&str>) -> Result<u32> {
        let mut stmt = transaction
            .prepare_cached(r#"
            select s.id, s.sticker_set 
            from sticker_file_ids f 
            join stickers s on (f.sticker_id = s.id) 
            where f.file_id = ?"#)?;
        let mut result = stmt.query_and_then(&[&file_id], |r| -> Result<(u32, Option<String>)> { Ok((r.get_checked(0)?, r.get_checked(1)?))})?;
        if let Some(row) = result.next() {
            let (sticker_id, stored_sticker_set): (u32, Option<String>) = row?;
            if stored_sticker_set.as_ref().map(String::as_str) != sticker_set {
                if let Some(sticker_set) = sticker_set {
                    transaction.execute("update stickers set sticker_set = ? where id = ?", &[&sticker_set, &sticker_id])?;
                }
            }
            Ok(sticker_id)
        } else {
            let sticker_id = transaction
                .prepare_cached("insert into stickers(sticker_set) values (?)")?
                .insert(&[&sticker_set])? as u32;

            transaction
                .prepare_cached(
                    "insert into sticker_file_ids(sticker_id, file_id) values (?, ?)",
                )?
                .execute(&[&sticker_id, &file_id])?;
            Ok(sticker_id)
        }
    }

    pub fn save_tags(
        &mut self,
        tags: Vec<String>,
        sticker_file_id: &str,
        sticker_set: Option<&str>,
        user_login: Option<&str>,
        user_id: String,
        when: u32,
    ) -> Result<Vec<String>> {
        {
            let transaction = self.connection.transaction()?;
            {
                let mut insert_tags = transaction.prepare_cached("insert into tags(name, lower) values (?, ?)",)?;
                for tag in tags.iter() {
                    insert_tags.execute(&[tag, &tag.to_lowercase()])?;
                }
                if let Some(user_login) = user_login {
                    transaction.execute("insert into users(tg_user_id, login) values (?, ?)", &[&user_id, &user_login])?;
                }
                let sticker_id = Db::get_sticker_id_by_file_id(&transaction, &sticker_file_id, sticker_set)?;

                let mut insert_stmt = transaction.prepare_cached(r#"
                insert into assignments (tag_id, sticker_id, user_id, timestamp)
                select tag_id, ?, ?, ? from tags where lower = ?
                    "#)?;
                for tag in tags {
                    insert_stmt.execute(&[&sticker_id, &user_id, &when, &tag.to_lowercase()])?;
                }
            }
            transaction.commit()?;
        }
        Ok(self.get_tags_by_sticker_id(&sticker_file_id)?)
    }

    pub fn get_tags_by_sticker_id(&self, sticker_file_id: &str,) -> Result<Vec<String>> {
        let mut stmt = self.connection.prepare_cached(r#"
        select t.name from tags t 
        left join assignments a using (tag_id)
        left join sticker_file_ids s using (sticker_id)
        where s.file_id = ?
        "#)?;
        let rows: ::std::result::Result<Vec<String>, _> = stmt.query_and_then(&[&sticker_file_id], |r| r.get_checked(0))?.collect();
        Ok(rows?)
    }

    pub fn find_sticker_file_ids_starting_from_tag(&self, tag: &str) -> Result<Vec<String>> {
        let mut escaped_tag: String = tag.to_lowercase().chars().flat_map(|c| match c {
            '%' => vec!['%', '%'],
            '_' => vec!['%', '_'],
            c => vec![c]
        }).collect();
        escaped_tag.push('%');

        let mut stmt = self.connection.prepare_cached(r#"
            select distinct s.file_id
            from sticker_file_ids s 
            left join assignments a using (sticker_id)
            left join tags t using(tag_id)
            where t.lower LIKE ? ESCAPE '%'
        "#)?;
        let rows: ::std::result::Result<Vec<String>, _> = stmt.query_and_then(&[&escaped_tag], |r| r.get_checked(0))?.collect();
        Ok(rows?)
    }
}

#[cfg(test)]
mod test {

    #[test]
    fn test_saving_and_getting_tags() {
        let mut d = super::Db::in_memory().unwrap();
        //let mut d = super::Db::new("test.db").unwrap();
        d.create_tables().unwrap();
        d.save_tags(
            vec!["Tag1", "Tag2", "Tagg3", "dtag", "ttag"]
                .into_iter()
                .map(|s| s.into())
                .collect(),
            "sticker_file_id".into(),
            None,
            Some("user_login".into()),
            "user_id".into(),
            1500000000,
        ).unwrap();        
        d.save_tags(
            vec!["Tog1", "Tog2", "Togg3", "dtog", "ttog"]
                .into_iter()
                .map(|s| s.into())
                .collect(),
            "sticker_file_id2".into(),
            Some("sset".into()),
            Some("user_login".into()),
            "user_id".into(),
            1500000000,
        ).unwrap();

        assert_eq!(d.get_tags_by_sticker_id("sticker_file_id".into()).unwrap(), vec!["Tag1", "Tag2", "Tagg3", "dtag", "ttag"]);
        assert_eq!(d.find_sticker_file_ids_starting_from_tag("Ta").unwrap(), vec!["sticker_file_id".to_string()]);
        assert_eq!(d.find_sticker_file_ids_starting_from_tag("to").unwrap(), vec!["sticker_file_id2".to_string()]);
    }

    #[test]
    fn update_sticker_set() {
        let mut d = super::Db::in_memory().unwrap();
        d.create_tables().unwrap();
        {
            let t = d.connection.transaction().unwrap();
            super::Db::get_sticker_id_by_file_id(&t, "404".into(), None).unwrap();
            t.commit().unwrap();
        }
        let stored_set: Option<String> = d.connection.query_row("select sticker_set from stickers", &[], |row| row.get(0)).unwrap();
        assert_eq!(stored_set, None);
        {
            let t = d.connection.transaction().unwrap();
            super::Db::get_sticker_id_by_file_id(&t, "404".into(), Some("set".into())).unwrap();
            t.commit().unwrap();  
        }
        let stored_set: Option<String> = d.connection.query_row("select sticker_set from stickers", &[], |row| row.get(0)).unwrap();
        assert_eq!(stored_set, Some("set".into()));
    }

}
