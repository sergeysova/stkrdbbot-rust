#[macro_use] 
extern crate error_chain;
extern crate rusqlite;
extern crate reqwest;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate log;
extern crate env_logger;

extern crate time;

mod types;
mod db;
mod tg;

use types::Result;

use std::collections::hash_set::HashSet;
use std::collections::hash_map::HashMap;

fn get_sticker_ids_from_tags(d: &db::Db, tags: String) -> Result<Vec<String>> {
    let mut sticker_ids: Option<HashSet<String>> = None;
    for tag in tags.split_whitespace() {
        let tag_stickers: HashSet<String> = d.find_sticker_file_ids_starting_from_tag(tag)?.into_iter().collect();
        match sticker_ids {
            None => sticker_ids = Some(tag_stickers),
            Some(collection) => sticker_ids = Some(&collection & &tag_stickers)
        }
    }
    Ok(sticker_ids.unwrap_or(HashSet::new()).into_iter().collect())
}

enum State {
    SentSticker{
        file_id: String,
        set_name: Option<String>
    }
}

fn read_key() -> String {
    use std::fs::File;
    use std::io::Read;

    let mut file = File::open("key.txt").expect("Key file");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("Read the key from file");

    contents
}

fn make_kb(tags: Vec<String>) -> serde_json::Value {
    json!({
        "inline_keyboard": [[{
            "text": "Try it!",
            "switch_inline_query": tags.join(" ")
        }]]
    })
}

fn digest_update(update: tg::Update, api: &tg::Api, db: &mut db::Db, conversations: &mut HashMap<u64, State>) -> Result<()> {
    match update {
        tg::Update::InlineQuery{id, offset, text} => {
            let result = get_sticker_ids_from_tags(&db, text).and_then(|sids|
            api.answer_inline_query_with_stickers(id, sids));
            if let Err(e) = result {
                error!("Could not fullfill request {}", e) // TODO: send me a message
            }
        },
        tg::Update::Message{text, chat_id, username} => {
            if text == "/cancel" {
                conversations.remove(&chat_id);
                api.send_message(chat_id, format!("Cancelled"), None)?;
                return Ok(())
            }
            if let Some(&State::SentSticker{file_id: ref sticker_id, ref set_name}) = conversations.get(&chat_id) {
                let tags_to_add: Vec<String> = text.split_whitespace().map(String::from).collect();
                if tags_to_add.len() == 0 {
                    api.send_message(chat_id, "No tags to add".into(), None)?;
                    return Ok(())
                }
                let all_tags = db.save_tags(
                    tags_to_add, sticker_id,
                    set_name.as_ref().map(String::as_str),
                    username.as_ref().map(String::as_str),
                    chat_id.to_string(),
                    time::now().to_timespec().sec as u32
                )?;
                api.send_message(chat_id, format!("You can now find this sticker by typing @stkrdbbot {}", all_tags.join(" ")), Some(make_kb(all_tags)))?;
            } else {
                api.send_message(chat_id, format!("Hi! Send me a sticker to add tags to it"), None)?;
            }
        },
        tg::Update::Sticker{file_id, chat_id, sticker_set} => {
            let tags = db.get_tags_by_sticker_id(&file_id)?;
            let msg = if tags.len() == 0 {
                format!("Send me words for this sticker {} from {:?} or /cancel", file_id, sticker_set)
            } else {
                format!("This sticker has been tagged with {:?}. Send me some words to add more or /cancel ({} from {:?})",
                tags.join(" "), file_id, sticker_set)
            };
            api.send_message(chat_id, msg, None)?;
            conversations.insert(chat_id, State::SentSticker{file_id, set_name: sticker_set});
        }
        _ => info!("Not implemented {:?}", update)
    }
    Ok(())
}

fn main() {
    env_logger::init().unwrap();

    let mut api = tg::Api::with_real_network(read_key()).expect("Unable to create API instance");
    let mut db = db::Db::new("stickers.db").expect("Unable to open db file");
    let mut conversations = HashMap::<u64, State>::new();
    loop {
        match api.get_updates() {
            Ok(updates) => {
                for update in updates {
                    info!("Got update {:?}", update);
                    let start_time = time::now();
                    if let Err(e) = digest_update(update, &api, &mut db, &mut conversations) {
                        error!("Digesting update: {}", e)
                    }
                    info!("Digested update in {}", time::now() - start_time);
                }
            },
            Err(e) => {
                error!("Error getting updates: {}", e);
                ::std::thread::sleep(::std::time::Duration::from_secs(1));
            }
        }
    }
}